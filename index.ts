import Server from './class/server';
import mongoose from 'mongoose';

import fileUpload from 'express-fileupload';

import userRoutes from './routes/user';
import homeRoutes from './routes/home';
import registerRoutes from './routes/register';

const server = new Server;


//file upload
server.app.use( fileUpload() );

//Routes
server.app.use( '/user', userRoutes);
server.app.use( '/home', homeRoutes);
server.app.use( '/register', registerRoutes);

// Connect to db
/*mongoose.connect('mongodb://localhost:27017/qr_u',
                 {useNewUrlParser: true, useCreateIndex: true}, ( err ) =>{

    if ( err ) throw err;
    console.log("Base de datos ONLINE");
});*/
mongoose.connect('mongodb+srv://dbAdmin:john123@cluster0.jcn03.mongodb.net/qru',
                {useNewUrlParser: true, useCreateIndex: true}, ( err ) =>{

    if ( err ) throw err;
        console.log("Base de datos ONLINE");
});


//go up express
server.start( () => {
    console.log(`Servidor corriendo en puerto ${server.port}`);
});
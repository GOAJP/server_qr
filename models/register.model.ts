import { Schema, model, Document } from 'mongoose';

const registerSchema = new Schema({
    teacher:{
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: [true, 'Debe existir una referencia a un usuario']
    },
    created: {
        type: String
    },
    entry_time: {
        type: String
    },
    departure_time: {
        type: String
    },
    num_hours: {
        type: String
    },
    status: {
        type: String,
        default: 'NA'
    }
});

interface Iregister extends Document{
    teacher: string,
    created: string,
    entry_time: string,
    departure_time: string,
    num_hours: string,
    status: string
}

export const Register = model<Iregister>('registers', registerSchema);
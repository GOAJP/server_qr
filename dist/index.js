"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./class/server"));
const mongoose_1 = __importDefault(require("mongoose"));
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const user_1 = __importDefault(require("./routes/user"));
const home_1 = __importDefault(require("./routes/home"));
const register_1 = __importDefault(require("./routes/register"));
const server = new server_1.default;
//file upload
server.app.use(express_fileupload_1.default());
//Routes
server.app.use('/user', user_1.default);
server.app.use('/home', home_1.default);
server.app.use('/register', register_1.default);
// Connect to db
/*mongoose.connect('mongodb://localhost:27017/qr_u',
                 {useNewUrlParser: true, useCreateIndex: true}, ( err ) =>{

    if ( err ) throw err;
    console.log("Base de datos ONLINE");
});*/
mongoose_1.default.connect('mongodb+srv://dbAdmin:john123@cluster0.jcn03.mongodb.net/qru', { useNewUrlParser: true, useCreateIndex: true }, (err) => {
    if (err)
        throw err;
    console.log("Base de datos ONLINE");
});
//go up express
server.start(() => {
    console.log(`Servidor corriendo en puerto ${server.port}`);
});

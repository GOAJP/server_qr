"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Register = void 0;
const mongoose_1 = require("mongoose");
const registerSchema = new mongoose_1.Schema({
    teacher: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'users',
        required: [true, 'Debe existir una referencia a un usuario']
    },
    created: {
        type: String
    },
    entry_time: {
        type: String
    },
    departure_time: {
        type: String
    },
    num_hours: {
        type: String
    },
    status: {
        type: String,
        default: 'NA'
    }
});
exports.Register = mongoose_1.model('registers', registerSchema);

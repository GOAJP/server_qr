"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const token_1 = __importDefault(require("../class/token"));
const homeRoutes = express_1.Router();
//Consult user
homeRoutes.get('/', (req, res) => {
    const tokenHome = token_1.default.getJwtToken({
        seed: 'UNIREMINGTON'
    });
    if (tokenHome) {
        res.status(200).json({
            ok: true,
            token: tokenHome
        });
    }
    else {
        res.status(400).json({
            ok: false,
            message: 'Error al obtener el token'
        });
    }
});
exports.default = homeRoutes;

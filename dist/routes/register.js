"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const register_model_1 = require("../models/register.model");
const autentication_1 = require("../middlewares/autentication");
const registerRoutes = express_1.Router();
//Create and Update register
registerRoutes.post('/create', autentication_1.verifyToken, (req, res) => {
    register_model_1.Register.find({ teacher: req.body.teacher, status: 'A' }, (err, registerDB) => {
        if (err)
            throw err;
        if (registerDB.length == 0) {
            const register = {
                teacher: req.body.teacher,
                created: req.body.created,
                entry_time: req.body.hour,
                departure_time: '',
                num_hours: '',
                status: 'A',
            };
            register_model_1.Register.create(register).then(registerDB => {
                res.status(200).json({
                    ok: true,
                    message: 'Registro creado exitosamente'
                });
            }).catch(err => {
                res.status(400).json({
                    ok: false,
                    err
                });
            });
        }
        else {
            const moment = require('moment');
            var entryHour = moment(registerDB[0].entry_time + 'am', 'hh:mm A');
            var exitHour = moment(req.body.hour + 'am', 'hh:mm A');
            var canHours = moment.duration(exitHour.diff(entryHour)).asHours();
            canHours = canHours.toFixed(2);
            const register = {
                departure_time: req.body.hour,
                num_hours: canHours,
                status: 'NA'
            };
            register_model_1.Register.findByIdAndUpdate(registerDB[0]._id, register, { new: true }, (err, userDB) => {
                if (err)
                    throw err;
                if (!userDB) {
                    return res.status(400).json({
                        ok: false,
                        message: 'No existe un docente con ese ID'
                    });
                }
                res.status(200).json({
                    ok: true,
                    token: 'Registro finalizado exitosamente'
                });
            });
        }
    });
});
//Consult registers activate
registerRoutes.get('/consult', autentication_1.verifyToken, (req, res) => {
    register_model_1.Register.find({ status: 'A' }, (err, registerDB) => {
        if (!registerDB) {
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }
        res.status(200).json({
            ok: true,
            registerDB
        });
    });
});
//Consult registers inactivate
registerRoutes.get('/consult/inactivate', autentication_1.verifyToken, (req, res) => {
    register_model_1.Register.find({ status: 'NA' }).populate('teacher').exec((err, registerDB) => {
        if (!registerDB) {
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }
        res.status(200).json({
            ok: true,
            registerDB
        });
    });
});
//Consult registers of teachers
registerRoutes.post('/consult/teachers', autentication_1.verifyToken, (req, res) => {
    register_model_1.Register.find({ teacher: req.body.teacher, status: 'NA' }, (err, registerDB) => {
        if (!registerDB) {
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }
        res.status(200).json({
            ok: true,
            registerDB
        });
    });
});
exports.default = registerRoutes;

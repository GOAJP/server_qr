"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_model_1 = require("../models/user.model");
const bcrypt_1 = __importDefault(require("bcrypt"));
const token_1 = __importDefault(require("../class/token"));
const autentication_1 = require("../middlewares/autentication");
const userRoutes = express_1.Router();
userRoutes.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
//Login
userRoutes.post('/login', (req, res) => {
    const body = req.body;
    user_model_1.User.findOne({ email: body.email }, (err, userDB) => {
        if (err)
            throw err;
        if (!userDB) {
            return res.json({
                ok: false,
                message: 'Usuario/Contraseña no son correctos'
            });
        }
        if (userDB.comparePassword(body.password)) {
            const tokenUser = token_1.default.getJwtToken({
                _id: userDB.id,
                name: userDB.name,
                lastName: userDB.lastName,
                email: userDB.email
            });
            res.status(200).json({
                ok: true,
                id: userDB.id,
                token: tokenUser
            });
        }
        else {
            return res.status(400).json({
                ok: false,
                message: 'Usuario/Contraseña no son correctos'
            });
        }
    });
});
//Create user
userRoutes.post('/create', (req, res) => {
    const user = {
        name: req.body.name,
        lastName: req.body.lastName,
        document_number: req.body.document_number,
        program: req.body.program,
        avatar: req.body.avatar,
        email: req.body.email,
        password: bcrypt_1.default.hashSync(req.body.password, 10),
    };
    user_model_1.User.create(user).then(userDB => {
        const tokenUser = token_1.default.getJwtToken({
            _id: userDB.id,
            name: userDB.name,
            lastName: userDB.lastName,
            email: userDB.email
        });
        res.status(200).json({
            ok: true,
            id: userDB.id,
            token: tokenUser
        });
    }).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
});
//Update user
userRoutes.put('/update', autentication_1.verifyToken, (req, res) => {
    const user = {
        name: req.body.name || req.user.name,
        email: req.body.email || req.user.email,
        avatar: req.body.avatar || req.user.avatar
    };
    user_model_1.User.findByIdAndUpdate(req.user._id, user, { new: true }, (err, userDB) => {
        if (err)
            throw err;
        if (!userDB) {
            return res.status(400).json({
                ok: false,
                message: 'No existe un usuario con ese ID'
            });
        }
        const tokenUser = token_1.default.getJwtToken({
            _id: userDB.id,
            name: userDB.name,
            email: userDB.email,
            avatar: userDB.avatar
        });
        res.status(200).json({
            ok: true,
            token: tokenUser
        });
    });
});
//Consult user
userRoutes.get('/', autentication_1.verifyToken, (req, res) => {
    const user = req.user;
    res.status(200).json({
        ok: true,
        user
    });
});
//Consult registers activate
userRoutes.post('/consult', autentication_1.verifyToken, (req, res) => {
    user_model_1.User.find({ _id: req.body.id }, (err, userDB) => {
        if (!userDB) {
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }
        res.status(200).json({
            ok: true,
            userDB
        });
    });
});
exports.default = userRoutes;

import express, {json, urlencoded} from 'express';
import cors from 'cors';

export default class Server {

    public app: express.Application;
    public port: number = 3000;

    constructor (){
        this.app = express();

        //Body parser
        this.app.use(urlencoded( {extended: true} ));
        this.app.use(json());

        //Cors configure
        this.app.use(cors({ origin: "*", credentials: true }) );
    }    

    start(callback: any){
        this.app.listen(process.env.PORT ?? this.port, callback );
    }
}
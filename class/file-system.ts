import { FileUpload } from '../interfaces/file-upload';

import path from 'path';
import fs from 'fs';
import uniqid from 'uniqid';


export default class FileSystem {

    constructor(){}

    saveImageTemp (file: FileUpload, userId: string){

        return new Promise( (resolve, reject) => {
            //Create folder
            const path = this.createFoder( userId );

            //file file
            const fileName = this.generateUniqueName( file.name );

            //move the file to folder of uploads
            file.mv( `${path}/${fileName}`, (err: any) => {

                if (err){
                    reject(err);
                } else {
                    resolve(err);
                }

            });
        });

    }

    private generateUniqueName(originalName: string){

        const nameArr = originalName.split('.');
        const extension = nameArr[ nameArr.length - 1 ];
        const idUnique = uniqid();

        return `${idUnique}.${extension}`;

    }

    private createFoder (userId: string){

        const pathUser = path.resolve( __dirname, '../uploads', userId );
        const pathUserTemp = pathUser + '/temp';

        const exist = fs.existsSync( pathUser );

        if (!exist){
            fs.mkdirSync( pathUser );
            fs.mkdirSync( pathUserTemp );
        }

        return pathUserTemp;

    }

    imagesFromTempToPost (userId: string) {

        const pathTemp = path.resolve(  __dirname, '../uploads/', userId, 'temp' );
        const pathPost = path.resolve(  __dirname, '../uploads/', userId, 'posts' );

        if ( !fs.existsSync( pathTemp ) ) {
            return [];
        }

        if ( !fs.existsSync( pathPost ) ) {
            fs.mkdirSync( pathPost );
        }

        const imagesTemp = this.getImages( userId );

        imagesTemp.forEach( image =>{
            fs.renameSync( `${ pathTemp }/${ image }`, `${ pathPost }/${ image }` );
        });

        return imagesTemp;
    }

    private getImages (userId: string){
        const pathTemp = path.resolve(  __dirname, '../uploads/', userId, 'temp' );

        return fs.readdirSync( pathTemp ) || [];
    }

    getPhotoUrl( userId: string, img:string ){

        //path photos
        const pathPhotos = path.resolve(  __dirname, '../uploads/', userId, 'posts', img );

        //if exist image
        const exist = fs.existsSync( pathPhotos );
        if (!exist){
            return path.resolve(  __dirname, '../assets/400x250.jpg');
        }

        return pathPhotos; 

    }
}
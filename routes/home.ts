import { Router, Request, Response } from 'express';
import Token from '../class/token';

const homeRoutes = Router();

//Consult user
homeRoutes.get('/', ( req: any, res: Response ) => {

    const tokenHome = Token.getJwtToken({
        seed: 'UNIREMINGTON'
    });

    if (tokenHome){
        res.status(200).json({
            ok: true,
            token: tokenHome
        });
    } else {
        res.status(400).json({
            ok: false,
            message: 'Error al obtener el token'
        });
    }   

});


export default homeRoutes;
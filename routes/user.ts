import { Router, Request, Response } from 'express';
import { User } from '../models/user.model';
import bcrypt from 'bcrypt';
import Token from '../class/token';
import { verifyToken } from '../middlewares/autentication';

const userRoutes = Router();

userRoutes.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//Login
userRoutes.post('/login', ( req: Request, res: Response ) => {

    const body = req.body;

    User.findOne({ email: body.email }, ( err, userDB ) =>{
        if ( err ) throw err;

        if (!userDB){
            return res.json({
                ok: false,
                message: 'Usuario/Contraseña no son correctos'
            });
        }

        if ( userDB.comparePassword(body.password) ){

            const tokenUser = Token.getJwtToken({
                _id: userDB.id,
                name: userDB.name,
                lastName: userDB.lastName,
                email: userDB.email
            });

            res.status(200).json({
                ok: true,
                id: userDB.id,
                token: tokenUser
            });

        } else{

            return res.status(400).json({
                ok: false,
                message: 'Usuario/Contraseña no son correctos'
            });
        }

    });
});


//Create user
userRoutes.post('/create', ( req: Request, res: Response ) => {

    const user = {
        name: req.body.name,
        lastName: req.body.lastName,
        document_number: req.body.document_number,
        program: req.body.program,
        avatar: req.body.avatar,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
    };

    User.create( user ).then( userDB  =>{

        const tokenUser = Token.getJwtToken({
            _id: userDB.id,
            name: userDB.name,
            lastName: userDB.lastName,
            email: userDB.email
        });

        res.status(200).json({
            ok: true,
            id: userDB.id,
            token: tokenUser
        });
        
    }).catch( err =>{
        res.json({
            ok: false,
            err
        });
    });

});

//Update user
userRoutes.put('/update', verifyToken, ( req: any, res: Response ) => {

    const user = {
        name: req.body.name || req.user.name,
        email: req.body.email || req.user.email,
        avatar: req.body.avatar || req.user.avatar
    }; 

    User.findByIdAndUpdate(req.user._id, user, {new: true}, (err, userDB) => {
        
        if (err) throw err;
        
        if (!userDB){
            return res.status(400).json({
                ok: false,
                message: 'No existe un usuario con ese ID'
            });
        }

        const tokenUser = Token.getJwtToken({
            _id: userDB.id,
            name: userDB.name,
            email: userDB.email,
            avatar: userDB.avatar
        });

        res.status(200).json({
            ok: true,
            token: tokenUser
        });

    });
});

//Consult user
userRoutes.get('/', verifyToken, ( req: any, res: Response ) => {

    const user = req.user;

    res.status(200).json({
        ok: true,
        user
    });

});

//Consult registers activate
userRoutes.post('/consult', verifyToken, ( req: any, res: Response ) => {

    User.find({ _id: req.body.id}, (err, userDB) => {

        if (!userDB){
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }

        res.status(200).json({
            ok: true,
            userDB
        });
    });

});


export default userRoutes;
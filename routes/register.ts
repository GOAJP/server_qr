import { Router, Request, Response } from 'express';
import { Register } from '../models/register.model';
import { verifyToken } from '../middlewares/autentication';

const registerRoutes = Router();

//Create and Update register
registerRoutes.post('/create', verifyToken, ( req: any, res: Response ) => {

    Register.find({teacher: req.body.teacher, status: 'A'}, (err, registerDB) => {

        if (err) throw err;

        if (registerDB.length == 0) {

            const register = {
                teacher: req.body.teacher,
                created: req.body.created,
                entry_time: req.body.hour,
                departure_time: '',
                num_hours: '',
                status: 'A',
            };
        
            Register.create( register ).then( registerDB  =>{
             
                res.status(200).json({
                    ok: true,
                    message: 'Registro creado exitosamente'
                });
                
            }).catch( err =>{
                res.status(400).json({
                    ok: false,
                    err
                });
            });
        } else {
            const moment = require('moment');
            
            var entryHour = moment(registerDB[0].entry_time+'am', 'hh:mm A');
            var exitHour = moment(req.body.hour+'am', 'hh:mm A');

            var canHours = moment.duration(exitHour.diff(entryHour)).asHours();
            canHours = canHours.toFixed(2);
            
            
            const register = {
                departure_time: req.body.hour,
                num_hours: canHours,
                status: 'NA'
            }; 

            Register.findByIdAndUpdate(registerDB[0]._id, register, {new: true}, (err, userDB) => {
                
                if (err) throw err;
                
                if (!userDB){
                    return res.status(400).json({
                        ok: false,
                        message: 'No existe un docente con ese ID'
                    });
                }

                res.status(200).json({
                    ok: true,
                    token: 'Registro finalizado exitosamente'
                });

            });
        }

    });
});

//Consult registers activate
registerRoutes.get('/consult', verifyToken, ( req: any, res: Response ) => {

    Register.find({ status: 'A'}, (err, registerDB) => {

        if (!registerDB){
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }

        res.status(200).json({
            ok: true,
            registerDB
        });
    });

});

//Consult registers inactivate
registerRoutes.get('/consult/inactivate', verifyToken, ( req: any, res: Response ) => {

    Register.find({ status: 'NA'}).populate('teacher').exec((err, registerDB) => {

        if (!registerDB){
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }

        res.status(200).json({
            ok: true,
            registerDB
        });
    });

});

//Consult registers of teachers
registerRoutes.post('/consult/teachers', verifyToken, ( req: any, res: Response ) => {

    Register.find({ teacher: req.body.teacher, status: 'NA'}, (err, registerDB) => {

        if (!registerDB){
            return res.status(400).json({
                ok: false,
                message: 'No hay usuarios activos.'
            });
        }

        res.status(200).json({
            ok: true,
            registerDB
        });
    });

});


export default registerRoutes;